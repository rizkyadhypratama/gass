Basic   = {
        "Token"                : "c81cbdca6f40bf8ecb674ac9db3244f01adbf0cb2287fb7911ff4597f61de51aa01ad326766172ce3cd09826780062e1",
        "Currency"             : "ltc",
        "Reset If Profit"      : 0.000001,
        "Reset If Lose"        : 0,
        "Reset If Win Streak"  : 2,
        "Reset If Lose Streak" : 0,

        "Target Balance"       : 1000,
        "Target Profit"        : 10,
        "Target Lose"          : 0,

        "Auto Change Betset"   : "ON",


        ## If Profit/Lose Target Reached
        "Re-Play"              : "ON",
        "Refresh Seed"         : "ON",

        # 10 digit/char (fill free)
        # fill with `0` if you want random seed

        "Seed"                 : 0
}

Betset  = [{
        "Name"                  : "1",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.0000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 5.0,
                                 "Max": 5.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.06,
        },{
        "Name"                  : "2",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.0000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 25.0,
                                 "Max": 25.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.25,
        },{
        "Name"                  : "3",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.0000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 17.0,
                                 "Max": 17.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.22,
        },{
        "Name"                  : "4",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.0000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 33.0,
                                 "Max": 33.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.45,
        },{
        "Name"                  : "5",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 22.0,
                                 "Max": 22.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.6,
        },{
        "Name"                  : "6",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.0000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 11.0,
                                 "Max": 11.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.15,
        },{
        "Name"                  : "7",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 22.0,
                                 "Max": 22.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.26,
        },{
        "Name"                  : "8",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.0000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 8.0,
                                 "Max": 8.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.09,
        },{
        "Name"                  : "9",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },

        "Base Bet"              : 0.000001,
        "Max Base Bet"          : 0,

        "Chance"                : {
                                 "Min": 22.0,
                                 "Max": 22.0
        },
        "HiLo"                  : "H",  # AUTO / H / L

        "Multiply If Win"       : 0,
        "Multiply If Lose"      : 1.264,
        },{
        "Name"                  : "10",
        "Auto Change Betset If" : {
                                 "Win Streak"  : 0,
                                 "Lose Streak" : 0,
                                 "Profit"      : 0.000001,
                                 "Lose"        : 0,

                                 "Reset Base Bet": "ON",
        },
]


#•••••••••••••••••••••••••••••••••••••••••••••••
#
#         BL`378_project - Dice (Stake)
#
#•••••••••••••••••••••••••••••••••••••••••••••••

__CONFIG_VERSION__ = 2.0
